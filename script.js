//0 - pole bez statku, 1 - pole ze statkiem, 2 - strzal niecelny, 3 - strzal celny
var c = document.getElementById("canvasPlayer");
var ctx = c.getContext("2d");

var ce = document.getElementById("canvasEnemy");
var ctxe = ce.getContext("2d");

var arrBoolsPlayer = [];
var arrBoolsEnemy = [];

var cellSize = 30;
var arraySizeBool = 12;
var shipSize = 2;
var shipOrient = "horizontal";
var selectedShip = document.getElementById("shipRb");

var playerScore = 0;
var enemyScore = 0;
var play = false;

window.onload = function () {
    createArrayBools();
    drawGrid();
};

//tworzę tablice do przechowywania stanów planszy
function createArrayBools() {
    arrBoolsPlayer = [];
    arrBoolsEnemy = [];
    //tworzę z arrBools i arrBoolsNew tablice dwuwymiarowe i wypełniam je stanami 0
    for (var x = 0; x < arraySizeBool; x++) {
        arrBoolsPlayer[x] = [];
        arrBoolsEnemy[x] = [];
        for (var y = 0; y < arraySizeBool; y++) {
            arrBoolsPlayer[x][y] = 0;
            arrBoolsEnemy[x][y] = 0;
        }
    }
}

//narysowanie linii
function drawGrid() {

    //rysuje pionowe linie
    for (var x = 0; x <= cellSize * cellSize; x = x + cellSize) {
        ctx.beginPath();
        ctx.moveTo(x, 0);
        ctx.lineTo(x, cellSize * cellSize);
        ctx.stroke();

        ctxe.beginPath();
        ctxe.moveTo(x, 0);
        ctxe.lineTo(x, cellSize * cellSize);
        ctxe.stroke();
    }

    //rysuje poziome linie
    for (var y = 0; y <= cellSize * cellSize; y = y + cellSize) {
        ctx.beginPath();
        ctx.moveTo(0, y);
        ctx.lineTo(cellSize * cellSize, y);
        ctx.stroke();

        ctxe.beginPath();
        ctxe.moveTo(0, y);
        ctxe.lineTo(cellSize * cellSize, y);
        ctxe.stroke();
    }

}

//pobieram rozmiar statku z wcisiętego radiobuttona w menu
function getSize(element) {
    selectedShip = element;
    shipSize = parseInt(selectedShip.value);
}

//pobieram orientację ustawienia statku z menu
function getOrient(element) {
    shipOrient = element.value;
}

//sprawdzenie pozycji kliknięcia myszy, wynik kliknięcia jest zaokrąglany np. jeśli klikniemy myszą w punkt (23, 67) to zostanie to zaokrąglone do (1,3)
var canvasEnemy = document.getElementById("canvasEnemy").getBoundingClientRect();
c.addEventListener("click", getClickPosition, false);
ce.addEventListener("click", getClickPosition, false);

function getClickPosition(e) {
    var xPosition = (Math.floor((e.clientX - canvasEnemy.left) / 30)) + 1;
    var yPosition = (Math.floor((e.clientY - canvasEnemy.top) / 30)) + 1;

    if (yPosition < 11) {
        if (play == true)
            shootPlayer(xPosition, yPosition);
    }
    else if (yPosition > 11) {
        yPosition = yPosition - 11;
        setShip(xPosition, yPosition, shipSize, shipOrient, arrBoolsPlayer, false)
    }
}

//strzelanie do przeciwnika, jesli cel trafiony to w tablicy arrBoolEnemy pole bedzie = 3, jesli nie trafiony to = 2
function shootPlayer(widthPointArr, heightPointArr) {
    var widthPointDraw = widthPointArr - 1;
    var heightPointDraw = heightPointArr - 1;

    if ((arrBoolsEnemy[widthPointArr][heightPointArr] == 0 || arrBoolsEnemy[widthPointArr][heightPointArr] == 1) && arrBoolsEnemy[widthPointArr][heightPointArr] != 2 && arrBoolsEnemy[widthPointArr][heightPointArr] != 3) {
        if (arrBoolsEnemy[widthPointArr][heightPointArr] == 0) {  //pudło
            ctxe.fillStyle = 'blue';
            ctxe.fillRect(widthPointDraw * cellSize + 4, heightPointDraw * cellSize + 4, cellSize - 8, cellSize - 8);
            arrBoolsEnemy[widthPointArr][heightPointArr] = 2;
        }
        else {
            ctxe.fillStyle = 'red';
            ctxe.fillRect(widthPointDraw * cellSize + 4, heightPointDraw * cellSize + 4, cellSize - 8, cellSize - 8);
            arrBoolsEnemy[widthPointArr][heightPointArr] = 3;
            playerScore += 1;
            $('#playerScore').text('GRACZ: ' + playerScore);
            if (playerScore >= 25)
                alert("WYGRAŁEŚ!")
        }
        if (playerScore < 25)
            shootEnemy();
    }
}

//strzelanie przeciwnika do gracza
function shootEnemy() {
    var shoot = false;

    while (!shoot) {
        var widthPointArr = randNumber(1, 10);
        var heightPointArr = randNumber(1, 10);

        var widthPointDraw = widthPointArr - 1;
        var heightPointDraw = heightPointArr - 1;

        if ((arrBoolsPlayer[widthPointArr][heightPointArr] == 0 || arrBoolsPlayer[widthPointArr][heightPointArr] == 1) && arrBoolsPlayer[widthPointArr][heightPointArr] != 2 && arrBoolsPlayer[widthPointArr][heightPointArr] != 3) {
            if (arrBoolsPlayer[widthPointArr][heightPointArr] == 0) {  //pudło
                ctx.fillStyle = 'blue';
                ctx.fillRect(widthPointDraw * cellSize + 4, heightPointDraw * cellSize + 4, cellSize - 8, cellSize - 8);
                arrBoolsPlayer[widthPointArr][heightPointArr] = 2;
            }
            else {
                ctx.fillStyle = 'red';
                ctx.fillRect(widthPointDraw * cellSize + 4, heightPointDraw * cellSize + 4, cellSize - 8, cellSize - 8);
                arrBoolsPlayer[widthPointArr][heightPointArr] = 3;
                enemyScore += 1;
                $('#enemyScore').text('KOMPUTER: ' + enemyScore);
                if (enemyScore >= 25)
                    alert("PRZEGRAŁEŚ!")
            }
            shoot = true;
        }
    }
}

//ustawianie statków na planszy gracza
function setShip(widthPointArr, heightPointArr, shipSize, shipOrient, arrBools, automat) {
    if ((!selectedShip.disabled && !automat) || automat) {
        var widthPointDraw = widthPointArr - 1;
        var heightPointDraw = heightPointArr - 1;
        var check = 0;

        if (shipOrient == "horizontal" && (widthPointArr + shipSize < 12)) {
            for (var sx = widthPointArr - 1; sx < widthPointArr + shipSize + 1; sx++) {
                for (var sy = heightPointArr - 1; sy < heightPointArr + 2; sy++) {
                    if (arrBools[sx][sy] == 1) {
                        check += 1;
                    }
                }
            }
        }
        else if (shipOrient == "vertical" && (heightPointArr + shipSize < 12)) {
            for (var sx = widthPointArr - 1; sx < widthPointArr + 2; sx++) {
                for (var sy = heightPointArr - 1; sy < heightPointArr + shipSize + 1; sy++) {
                    if (arrBools[sx][sy] == 1) {
                        check += 1;
                    }
                }
            }
        }
        else if (shipOrient == null) {
            alert("Nie można ustawiac ręcznie statków w trybie automatycznym");
        }
        else {
            if (!automat)
                alert("W tym miejscu nie można ustawić statku");
            return false;
        }

        if (arrBools[widthPointArr][heightPointArr] == 0 && check == 0) {
            for (var i = 0; i < shipSize; i++) {
                if (shipOrient == "horizontal") {
                    if (arrBools == arrBoolsPlayer) {
                        ctx.fillStyle = 'black';
                        ctx.fillRect(((widthPointDraw + i) * cellSize + 1), (heightPointDraw * cellSize + 1), cellSize - 2, cellSize - 2);
                    }
                    else {
                        //ctxe.fillStyle = 'black';
                        //ctxe.fillRect(((widthPointDraw + i) * cellSize + 1), (heightPointDraw * cellSize + 1), cellSize - 2, cellSize - 2);
                    }
                    arrBools[widthPointArr + i][heightPointArr] = 1;
                }
                else if (shipOrient == "vertical") {
                    if (arrBools == arrBoolsPlayer) {
                        ctx.fillStyle = 'black';
                        ctx.fillRect((widthPointDraw * cellSize + 1), ((heightPointDraw + i) * cellSize + 1), cellSize - 2, cellSize - 2);
                    }
                    else {
                        //ctxe.fillStyle = 'black';
                        //ctxe.fillRect((widthPointDraw * cellSize + 1), ((heightPointDraw + i) * cellSize + 1), cellSize - 2, cellSize - 2);
                    }
                    arrBools[widthPointArr][heightPointArr + i] = 1;
                }
            }
            if (!selectedShip.disabled && !automat)
                selectedShip.disabled = true;
            return true;
        }
        else {
            if (!automat)
                alert("W tym miejscu nie można ustawić statku");
            return false;
        }
    }
}

//losowanie pozycji statków
function randShipPosition(arrBools) {
    var shipsArr = [7, 4, 4, 3, 3, 2, 2];
    var orientArr = ["horizontal", "vertical"];
    for (var i = 0; i < shipsArr.length; i++) {
        var xPos = randNumber(1, 10);
        var yPos = randNumber(1, 10);
        var orient = randNumber(0, 1);
        if (!setShip(xPos, yPos, shipsArr[i], orientArr[orient], arrBools, true)) {
            i -= 1;
        }
    }
}

function randNumber(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

function resetAll() {
    play = false;
    playerScore = 0;
    enemyScore = 0;
    for (var x = 0; x < arraySizeBool; x++) {
        for (var y = 0; y < arraySizeBool; y++) {
            arrBoolsPlayer[x][y] = 0;
            arrBoolsEnemy[x][y] = 0;
        }
    }

    ctx.clearRect(0, 0, 300, 300);
    ctxe.clearRect(0, 0, 300, 300);

    drawGrid();
    $('#startGame').attr('disabled', false);
    shipSize = parseInt($('input[name="shipSelect"]:checked').val());
    shipOrient = $('input[name="orientSelect"]:checked').val();
    $('input[name="shipSelect"]').attr('disabled', false);
    $('input[name="orientSelect"]').attr('disabled', false);
}

function startGame() {
    if ($('input[name="shipSelect"]').is(':disabled') && !play) {
        randShipPosition(arrBoolsEnemy);
        play = true;
        $('#startGame').attr('disabled', true);
    }

}

function buttonManager() {
    resetAll();
    if (document.getElementById('manual').checked) {
        shipSize = parseInt($('input[name="shipSelect"]:checked').val());
        shipOrient = $('input[name="orientSelect"]:checked').val();
        $('input[name="shipSelect"]').attr('disabled', false);
        $('input[name="orientSelect"]').attr('disabled', false);

    } else {
        shipSize = null;
        shipOrient = null;
        randShipPosition(arrBoolsPlayer);
        $('input[name="shipSelect"]').attr('disabled', true);
        $('input[name="orientSelect"]').attr('disabled', true);
    }
}